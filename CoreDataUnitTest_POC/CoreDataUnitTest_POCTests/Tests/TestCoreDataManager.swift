//
//  TestCoreDataManager.swift
//  CoreDataUnitTest_POCTests
//
//  Created by Sourab Biswas on 08/06/21.
//

import XCTest
import CoreData
@testable import CoreDataUnitTest_POC

class TestCoreDataManager: XCTestCase {
    
    var coreDataStack: CoreDataTestStack!
    var manager: CoreDataManager!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        coreDataStack = CoreDataTestStack()
        manager = CoreDataManager(coreDataStack.mainContext)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_create_record() -> Void {
        let _ = manager.createRecord(firstName: "Test", lastName: "User", age: "12")
        let personalData = manager.fetchGeneralRecords()
        let record = personalData!.filter({$0.firstName == "Test"})
        XCTAssertNotNil(record, "Records is Nil")
    }
    
    func test_fetch_record() -> Void {
        //let mainContext = coreDataStack.mainContext
        let _ = manager.createRecord(firstName: "Test", lastName: "User", age: "12")
        let record = manager.fetchAllRecordsOf("GeneralRecord") as? [GeneralRecord]
        XCTAssertEqual(record?.count, 1)
    }
    
    func test_delete_record() -> Void {
        //let mainContext = coreDataStack.mainContext
        let data1 = manager.createRecord(firstName: "Test1", lastName: "User", age: "12")
        let data2 = manager.createRecord(firstName: "Test2", lastName: "User", age: "13")
        let data3 = manager.createRecord(firstName: "Test3", lastName: "User", age: "14")
        
        manager.deleteGeneralRecordFor(data2!) { isSuccess in
            if isSuccess {
                print("Deleted")
            }
        }
        
        let allData = manager.fetchGeneralRecords()
        XCTAssertEqual(allData?.count, 2)
        XCTAssertTrue(allData!.contains(data1!))
        XCTAssertTrue(allData!.contains(data3!))
    }
    
    func test_delete_record_thatNot_exsist() -> Void {
        let personalData = GeneralRecord(context: coreDataStack.backgroundContext) // = NSEntityDescription.insertNewObject(forEntityName: "GeneralRecord", into: coreDataStack.mainContext) as! GeneralRecord
        personalData.firstName = "Sourab"
        personalData.lastName = "Biswas"
        personalData.age = 12
        manager.deleteGeneralRecordFor(personalData) { isSuccess in
            XCTAssertFalse(isSuccess)
        }
    }

}
