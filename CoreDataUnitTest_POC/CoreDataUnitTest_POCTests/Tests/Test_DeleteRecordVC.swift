//
//  Test_DeleteRecordVC.swift
//  CoreDataUnitTest_POCTests
//
//  Created by Sourab Biswas on 09/06/21.
//

import XCTest
import CoreData

@testable import CoreDataUnitTest_POC

class Test_DeleteRecordVC: XCTestCase {

    var storyboard: UIStoryboard!
    var deleteRecordVC: DeleteRecordVC!
    var coreDataStack: CoreDataTestStack!
    var manager: CoreDataManager!
    
    var exp = XCTestExpectation()
    var exp1 = XCTestExpectation()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        coreDataStack = CoreDataTestStack()
        manager = CoreDataManager(coreDataStack.mainContext)
        storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        deleteRecordVC = storyboard.instantiateViewController(identifier: "DeleteRecordVC") as? DeleteRecordVC
        deleteRecordVC.viewDidLoad()
        
        //exp = self.expectation(description: "test")

    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_viewDidLoad() {
        //Arrange
        
        //Act
        deleteRecordVC?.viewDidLoad()
        
        //Asserts
        XCTAssertEqual(deleteRecordVC?.title, "Delete Record")
    }
    
    func test_deleteRecord_failure() {
        //Arrange
        let personalData = GeneralRecord(context: coreDataStack.backgroundContext) // = NSEntityDescription.insertNewObject(forEntityName: "GeneralRecord", into: coreDataStack.mainContext) as! GeneralRecord
        personalData.firstName = "Sourab1"
        personalData.lastName = "Biswas1"
        personalData.age = 22
        
        var result = false
        //Act
        deleteRecordVC?.deleteRecord(personalData) { isSuccess in
            result = isSuccess
        }
        
        //Asserts
        XCTAssertFalse(result)
    }
    
    func test_deleteRecord_success() {
        //Arrange
        let data1 = manager.createRecord(firstName: "Test1", lastName: "User", age: "12", coreDataStack.mainContext)
        deleteRecordVC?.coreDataManager.mainContext = coreDataStack.mainContext
        var result = false
        
        //Act
        deleteRecordVC?.deleteRecord(data1!) { isSuccess in
            result = isSuccess
        }
        
        //Asserts
        XCTAssertTrue(result)
    }

    func test_hasATableView() {
        XCTAssertNotNil(deleteRecordVC?.tableView)
    }
    
    func test_tableViewHasDelegate() {
        XCTAssertNotNil(deleteRecordVC?.tableView!.delegate)
    }
    
    func test_numberOfRowsInSection_tableViewDelegate() {
        //Arrange
        
        //Act
        let _ = manager.createRecord(firstName: "Test", lastName: "User", age: "12")
        let personalData = manager.fetchGeneralRecords()
        deleteRecordVC.personalRecords = personalData
        
        //Assert
        XCTAssertEqual(1, deleteRecordVC.tableView!.dataSource?.tableView(deleteRecordVC.tableView!, numberOfRowsInSection: 0))
    }
    
    func test_cellForRowAt_tableViewDelegate() {
        //Arrange
        let _ = manager.createRecord(firstName: "Sourab", lastName: "Biswas", age: "12")
        let personalData = manager.fetchGeneralRecords()
        deleteRecordVC.personalRecords = personalData
        //Act
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = deleteRecordVC.tableView!.dataSource?.tableView(deleteRecordVC.tableView!, cellForRowAt: indexPath)
        let data = deleteRecordVC.personalRecords?[indexPath.row]
        //Assert
        XCTAssertEqual("Sourab", data?.firstName)
        XCTAssertEqual("Sourab Biswas", cell?.textLabel?.text)
        XCTAssertEqual(1, deleteRecordVC.personalRecords?.count)
    }
    
    func test_canEditRowAt_tableViewDelegate() -> Void {
        //Arrange
        let indexPath = IndexPath(row: 0, section: 0)
        
        //Act
        
        //Assert
        XCTAssertTrue(deleteRecordVC.tableView!.dataSource!.tableView!(deleteRecordVC.tableView!, canEditRowAt: indexPath))
    }
    
    func test_editingStyleHandling_tableViewDelegate() -> Void {
        //Arrange
        let indexPath = IndexPath(row: 0, section: 0)
        let style = UITableViewCell.EditingStyle.delete
        
        deleteRecordVC.coreDataManager.mainContext = coreDataStack.mainContext
        let _ = manager.createRecord(firstName: "Sourab", lastName: "Biswas", age: "12")
        //Act
        
        deleteRecordVC.tableView?.dataSource!.tableView!(deleteRecordVC.tableView!, commit: style, forRowAt: indexPath)
        
        //Assert
        XCTAssertEqual(0, deleteRecordVC.personalRecords?.count)
    }
    
    func test_editingStyleHandling_tableViewDelegate_false() -> Void {
        let indexPath = IndexPath(row: 0, section: 0)
        let style = UITableViewCell.EditingStyle.delete
        
        deleteRecordVC.coreDataManager.mainContext = coreDataStack.mainContext
        let personalData = GeneralRecord(context: coreDataStack.backgroundContext) // = NSEntityDescription.insertNewObject(forEntityName: "GeneralRecord", into: coreDataStack.mainContext) as! GeneralRecord
        personalData.firstName = "Sourab"
        personalData.lastName = "Biswas"
        personalData.age = 12
        deleteRecordVC.personalRecords?.append(personalData)
        //Act
        
        deleteRecordVC.tableView?.dataSource!.tableView!(deleteRecordVC.tableView!, commit: style, forRowAt: indexPath)
        
        //Assert
        XCTAssertGreaterThan(deleteRecordVC.personalRecords!.count, 0)
    }
    
    func test_nilCheck() -> Void {
        let _ = manager.createRecord(firstName: nil, lastName: nil, age: nil)
        let personalData = manager.fetchGeneralRecords()
        deleteRecordVC.personalRecords = personalData
        //Act
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = deleteRecordVC.tableView!.dataSource?.tableView(deleteRecordVC.tableView!, cellForRowAt: indexPath)
        let data = deleteRecordVC.personalRecords?[indexPath.row]
        //Assert
//        XCTAssertEqual("Sourab", data?.firstName)
//        XCTAssertEqual("Sourab Biswas", cell?.textLabel?.text)
//        XCTAssertEqual(1, deleteRecordVC.personalRecords?.count)
    }
    
    func test_tsetSimple() {
        
        exp = self.expectation(description: "test")
        exp1 = self.expectation(description: "test1")
        deleteRecordVC.testSimple()
        
        let seconds = 1.2
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.exp.fulfill()
        }
        
        let seconds1 = 2.2
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds1) {
            self.exp1.fulfill()
            
        }
        
        //waitForExpectations(timeout: seconds, handler: nil)
        
        wait(for: [self.exp], timeout: seconds)
        wait(for: [self.exp1], timeout: seconds1)
    }
}
