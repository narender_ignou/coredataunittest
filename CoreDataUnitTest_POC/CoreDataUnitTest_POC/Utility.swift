//
//  Utility.swift
//  Advantage
//
//  Created by Shreehari Bhat on 5/06/20.
//  Copyright © 2020 Shreehari Bhat. All rights reserved.
//

import Foundation
import UIKit
import CoreData

struct Utility {
    // MARK:- Loader methods
    
    static var statesArray : [[String : Any]] = [
      [ "s_id" : 1,  "name" : "OH"   ],
      [ "s_id" : 2,  "name" : "WI"   ],
      [ "s_id" : 3,  "name" : "NV"   ],
      [ "s_id" : 5,  "name" : "AL"   ],
      [ "s_id" : 6,  "name" : "AK"   ],
      [ "s_id" : 7,  "name" : "AZ"   ],
      [ "s_id" : 8,  "name" : "AR"   ],
      [ "s_id" : 9,  "name" : "CA"   ],
      [ "s_id" : 10, "name" : "CO"   ],
      [ "s_id" : 11, "name" : "CT"   ],
      [ "s_id" : 12, "name" : "DE"   ],
      [ "s_id" : 13, "name" : "FL"   ],
      [ "s_id" : 14, "name" : "GA"   ],
      [ "s_id" : 15, "name" : "HI"   ],
      [ "s_id" : 16, "name" : "ID"   ],
      [ "s_id" : 17, "name" : "IL"   ],
      [ "s_id" : 18, "name" : "IN"   ],
      [ "s_id" : 19, "name" : "IA"   ],
      [ "s_id" : 20, "name" : "KS"   ],
      [ "s_id" : 21, "name" : "KY"   ],
      [ "s_id" : 22, "name" : "LA"   ],
      [ "s_id" : 23, "name" : "ME"   ],
      [ "s_id" : 24, "name" : "MD"   ],
      [ "s_id" : 25, "name" : "MA"   ],
      [ "s_id" : 26, "name" : "MI"   ],
      [ "s_id" : 27, "name" : "MN"   ],
      [ "s_id" : 28, "name" : "MS"   ],
      [ "s_id" : 29, "name" : "MO"   ],
      [ "s_id" : 30, "name" : "MT"   ],
      [ "s_id" : 31, "name" : "NE"   ],
      [ "s_id" : 32, "name" : "NH"   ],
      [ "s_id" : 33, "name" : "NJ"   ],
      [ "s_id" : 34, "name" : "NM"   ],
      [ "s_id" : 35, "name" : "NY"   ],
      [ "s_id" : 36, "name" : "NC"   ],
      [ "s_id" : 37, "name" : "ND"   ],
      [ "s_id" : 38, "name" : "OK"   ],
      [ "s_id" : 39, "name" : "OR"   ],
      [ "s_id" : 40, "name" : "PA"   ],
      [ "s_id" : 41, "name" : "RI"   ],
      [ "s_id" : 42, "name" : "SC"   ],
      [ "s_id" : 43, "name" : "SD"   ],
      [ "s_id" : 44, "name" : "TN"   ],
      [ "s_id" : 45, "name" : "TX"   ],
      [ "s_id" : 46, "name" : "UT"   ],
      [ "s_id" : 47, "name" : "VT"   ],
      [ "s_id" : 48, "name" : "VA"   ],
      [ "s_id" : 49, "name" : "WA"   ],
      [ "s_id" : 50, "name" : "WV"   ],
      [ "s_id" : 51, "name" : "WY"   ],
      [ "s_id" : 52, "name" : "AS"   ],
      [ "s_id" : 53, "name" : "DC"   ],
      [ "s_id" : 54, "name" : "FM"   ],
      [ "s_id" : 55, "name" : "GU"   ],
      [ "s_id" : 56, "name" : "MH"   ],
      [ "s_id" : 57, "name" : "MP"   ],
      [ "s_id" : 58, "name" : "PW"   ],
      [ "s_id" : 59, "name" : "PR"   ],
      [ "s_id" : 60, "name" : "VI"   ],
      [ "s_id" : 61, "name" : "AE-A" ],
      [ "s_id" : 62, "name" : "AA"   ],
      [ "s_id" : 63, "name" : "AE-C" ],
      [ "s_id" : 64, "name" : "AE-E" ],
      [ "s_id" : 65, "name" : "AE-ME"],
      [ "s_id" : 66, "name" : "AP"   ],
      [ "s_id" : 68, "name" : "VIC"  ],
      [ "s_id" : 69, "name" : "BS"   ],
      [ "s_id" : 71, "name" : "QL"   ]
    ]
    static func showToastNotification(_ message: String) {
        
    }
    
    //MARK:- JSON
    static func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    static func validateUserName(name: String) ->Bool {
        // Length be 15 characters max and 2 characters minimum.
        let nameRegex = "^\\w{2,15}$"
        let trimmedString = name.trimmingCharacters(in: .whitespaces)
        let validateName = NSPredicate(format: "SELF MATCHES %@", nameRegex)
        let isValidateName = validateName.evaluate(with: trimmedString)
        return isValidateName
    }
    
    //MARK:- APPINFO
    static let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    static let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String

    static func deviceID() -> String {
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        return deviceID!
    }
    // Global variables
    static var defaultAPIServiceHeaders: [String: String] {
        var headers: [String: String] = [String: String]()
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"
        return headers
    }
    
    // Get User Data

    
    // Fetch Records with Entity
    static func fetchAllRecords(_ entityName : String) -> [AnyObject] {
        let context = AppDelegate.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)

        var fetchedArray : [AnyObject] = []
        do {
            let results = try context.fetch(fetchRequest)
            for obj in results {
                fetchedArray.append(obj as AnyObject)
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }
        return fetchedArray
    }
    
    static func fetchAllRecords(_ entityName : String, withPredicate iPredicate : NSPredicate?) -> [AnyObject] {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = iPredicate
        fetchRequest.returnsObjectsAsFaults = false
        var fetchedArray : [AnyObject] = []
        do {
            let results = try context.fetch(fetchRequest)
            for obj in results {
                fetchedArray.append(obj as AnyObject)
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }
        return fetchedArray
    }
    
    // Clear record
    static func clearRecords(_ entityName : String) {
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: AppDelegate.shared.persistentContainer.viewContext)
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = entityDescription
        do {
            let fetchedEntities = try AppDelegate.shared.persistentContainer.viewContext.fetch(request)
            if fetchedEntities.count > 0 {
                for entity in fetchedEntities {
                    AppDelegate.shared.persistentContainer.viewContext.delete(entity as! NSManagedObject)
                }
            }
            //AppDelegate.shared.saveContext()
            try? AppDelegate.shared.persistentContainer.viewContext.save()
            //try context.save()
        } catch {
            print(error)
        }
    }
    
    // Show Alert With String
    static func showAlertWithString(_ message : String) -> UIAlertController {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) {(alert: UIAlertAction!) in
            // Ok Action Handler
        }
        alert.addAction(okAction)
        return alert
    }
    
    static func showAlertwithString(_ message: String?, withTitle title: String?, otherButton okButton: String?) -> UIAlertController? {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesButton = UIAlertAction(title: okButton, style: .default, handler: { action in
                //Handle your yes please button action here
            })
        alert.addAction(yesButton)
        return alert
    }
    
    
    static func isFirstLaunch() -> Bool {
        if let result = UserDefaults.standard.object(forKey: "isFirstLaunch") {
            return (result as! Bool)
        } else {
            return false
        }
    }
    
    static func getFormattedPhoneNumber(_ number : String) -> String {
        if number.count == 10 {
            return "(\(self.breakedNumber(startOffSet: 0, endOffSet: -7, number: number))) \(self.breakedNumber(startOffSet: 3, endOffSet: -4, number: number))-\(self.breakedNumber(startOffSet: 6, endOffSet: 0, number: number))"
        } else {
            return number
        }
    }
    
    fileprivate static func breakedNumber(startOffSet : Int, endOffSet : Int, number : String) -> String {
        let start = number.index(number.startIndex, offsetBy: startOffSet)
        let end = number.index(number.endIndex, offsetBy: endOffSet)
        let range = start..<end
        return String(number[range])
    }
    //MARK:- PHONE NUMBER FORMATTING UTILITY METHODS
    
    static func formatNumber(_ mobileNumber : String) -> String {
        var formattedPhoneNumber = self.removePhoneNumberFormat(mobileNumber)
        let length = formattedPhoneNumber.count
        if length > 10 {
            let fromIndex = formattedPhoneNumber.index(formattedPhoneNumber.startIndex, offsetBy: length-10)
            formattedPhoneNumber = String(formattedPhoneNumber[fromIndex...])
        }
        return formattedPhoneNumber;
    }
    
    static func getLength(_ mobileNumber : String) -> Int {
        let formattedPhoneNumber = self.removePhoneNumberFormat(mobileNumber)
        return formattedPhoneNumber.count
    }
    
    static func removePhoneNumberFormat(_ fetchedText : String) -> String {
        var returnFormattedPhoneNumber = fetchedText
        returnFormattedPhoneNumber = returnFormattedPhoneNumber.replacingOccurrences(of: "(", with: "")
        returnFormattedPhoneNumber = returnFormattedPhoneNumber.replacingOccurrences(of: ")", with: "")
        returnFormattedPhoneNumber = returnFormattedPhoneNumber.replacingOccurrences(of: " ", with: "")
        returnFormattedPhoneNumber = returnFormattedPhoneNumber.replacingOccurrences(of: "-", with: "")
        returnFormattedPhoneNumber = returnFormattedPhoneNumber.replacingOccurrences(of: "+", with: "")
        return returnFormattedPhoneNumber
    }
    
    static func validateEmailWithString(_ email : String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: email)
    }
    
    static func calcAge(birthday: String) -> Int {
        //"2020-07-16T00:00:00"
        // yyyy-MM-dd'T'HH:mm:ssZ
        let dateFormater = DateFormatter()
        dateFormater.locale = Locale(identifier: "en_US_POSIX")
        dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let birthdayDate = dateFormater.date(from: birthday) {
            let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
            let now = Date()
            let calcAge = calendar.components(.year, from: birthdayDate, to: now, options: [])
            let age = calcAge.year ?? 0
            return age
        } else {
            return 0
        }
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func convertIntoRequiredDateFormat(_ dateStr : String, needUTC isUTCEnabled : Bool) -> String? {
        let dateFormatter = DateFormatter()
        if dateStr.count == 25 {
            if dateStr.contains("T") {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            } else {
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            }
        } else if dateStr.count == 19 {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        } else if dateStr.count == 10 {
            dateFormatter.dateFormat = "dd-MM-yyyy"
//            dateFormatter.dateFormat = "MM-dd-yyyy"
        }
        if dateFormatter.date(from:dateStr) != nil {
            let pureDate = dateFormatter.date(from:dateStr)!
            let formatter = DateFormatter()
            if isUTCEnabled {
                formatter.timeZone = TimeZone(abbreviation: "UTC")
            }
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            return formatter.string(from: pureDate)
        } else {return nil}
    }
}
