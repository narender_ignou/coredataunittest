//
//  CreateRecordVC.swift
//  CoreDataUnitTest_POC
//
//  Created by Sourab Biswas on 07/06/21.
//

import UIKit
import CoreData

class CreateRecordVC: UIViewController {

    @IBOutlet weak var fNameTextField: UITextField!
    @IBOutlet weak var lNameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.endEditing(_:)))
        gesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(gesture)
        self.title = "Create Record"
    }
    
    @objc func endEditing(_ gesture: UITapGestureRecognizer) -> Void {
        self.view.endEditing(true)
    }
    
    override func viewWillLayoutSubviews() {
        self.textFieldSetUp(self.fNameTextField)
        self.textFieldSetUp(self.lNameTextField)
        self.textFieldSetUp(self.ageTextField)
        self.saveButton.layer.cornerRadius = 10.0
        self.saveButton.backgroundColor = .darkGray
        self.saveButton.setTitleColor(.white, for: .normal)
    }
    
    func textFieldSetUp(_ textField: UITextField) -> Void {
        textField.layer.cornerRadius = 5.0
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    @IBAction func saveRecordAction(_ sender: Any) {
        self.view.endEditing(true)
        if self.fNameTextField.text!.count > 0 && self.lNameTextField.text!.count > 0 && self.ageTextField.text!.count > 0 {
            self.insertDataIntoCoreData()
        } else {
            self.present(Utility.showAlertWithString("All Fields are required!!!"), animated: true, completion: nil)
        }
    }
    
    
    func insertDataIntoCoreData() -> Void
    {
        let manager = CoreDataManager()
        let generalRecord = manager.createRecord(firstName: self.fNameTextField.text, lastName: self.lNameTextField.text, age: self.ageTextField.text)
        
        if let validController = self.storyboard?.instantiateViewController(identifier: "DeleteRecordVC") as? DeleteRecordVC {
            print("First Name: \(generalRecord?.firstName ?? ""), Last Name: \(generalRecord?.lastName ?? ""), Age: \(generalRecord?.age ?? 0)")
            self.fNameTextField.text = ""
            self.lNameTextField.text = ""
            self.ageTextField.text = ""
            self.navigationController?.pushViewController(validController, animated: true)
        }
    }
}

