//
//  CoreDataManager.swift
//  CoreDataUnitTest_POC
//
//  Created by Sourab Biswas on 08/06/21.
//

import Foundation
import CoreData

class CoreDataManager {
    
    var mainContext: NSManagedObjectContext? = nil
    
    convenience init(_ mainContext: NSManagedObjectContext) {
        self.init()
        self.mainContext = mainContext
    }
    
    init() {
    }
    
    func createRecord(firstName: String?, lastName: String?, age: String?, _ context: NSManagedObjectContext = AppDelegate.shared.persistentContainer.viewContext) -> GeneralRecord? {
        let testContext = self.mainContext == nil ? context : self.mainContext
        let generalRecord = GeneralRecord(context: testContext!)
        generalRecord.firstName = firstName
        generalRecord.lastName = lastName
        generalRecord.age = Int16(age ?? "0")!
        do {
            try testContext!.save()
            return generalRecord
        } catch let error {
            print("Failed to Create Record: \(error.localizedDescription)")
        }
        return nil
    }
    
    func fetchGeneralRecords(_ context: NSManagedObjectContext = AppDelegate.shared.persistentContainer.viewContext) -> [GeneralRecord]? {
        let testContext = self.mainContext == nil ? context : self.mainContext
        let fetchRequest = NSFetchRequest<GeneralRecord>(entityName: "GeneralRecord")
        do {
            let records = try testContext!.fetch(fetchRequest)
            return records
        } catch let error {
            print("Failed to fetch General Records because of \(error.localizedDescription)")
        }
        return nil
    }
    
    func fetchAllRecordsOf(_ entityName: String, _ context: NSManagedObjectContext = AppDelegate.shared.persistentContainer.viewContext) -> [Any]? {
        let testContext = self.mainContext == nil ? context : self.mainContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        do {
            let results = try testContext!.fetch(fetchRequest)
            return results
        }catch let err as NSError {
            print("Failed to fetch records for \(entityName) because of \(err.debugDescription)")
        }
        return nil
    }
    
    func deleteGeneralRecordFor(_ details: GeneralRecord,  _ context: NSManagedObjectContext = AppDelegate.shared.persistentContainer.viewContext ,_ completionHandler: @escaping (Bool) -> Void) {
        let testContext = self.mainContext == nil ? context : self.mainContext
        
        let allRecs = self.fetchGeneralRecords(testContext!)?.filter({$0 == details})
        if let validData = allRecs, validData.count > 0 {
            testContext!.delete(details as NSManagedObject)
            do {
                try testContext!.save()
                completionHandler(true)
            } catch {
                completionHandler(false)
            }
        } else {
            completionHandler(false)
        }
    } 
}
