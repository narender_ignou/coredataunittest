//
//  DeleteRecordVC.swift
//  CoreDataUnitTest_POC
//
//  Created by Sourab Biswas on 07/06/21.
//

import UIKit
import CoreData

class DeleteRecordVC: UIViewController {
    
    var tableView: UITableView?
    
    lazy var personalRecords: [GeneralRecord]? = {
        var generalRecords = coreDataManager.fetchGeneralRecords()
        generalRecords?.reverse()
        return generalRecords
    }()
    
    lazy var coreDataManager: CoreDataManager = {
        let manager = CoreDataManager()
        return manager
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Delete Record"
        
        self.tableView = UITableView(frame: self.view.bounds)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.view.addSubview(self.tableView!)
    }
    
    func deleteRecord(_ details: GeneralRecord,_ completionHandler: @escaping (Bool) -> Void) {
        self.coreDataManager.deleteGeneralRecordFor(details) { isSuccess in
            if isSuccess {
                completionHandler(isSuccess)
            } else {
                completionHandler(isSuccess)
            }
        }
    }
    
    //--------------------------------//
    func testSimple() {
        self.testWithClouser1("One") { (isSuccess) in
            if isSuccess {
                print("isSuccess true")
            } else {
                print("isSuccess false")
            }
        }
        
        self.testWithClouser("two") { (isSuccess) in
            if isSuccess {
                print("isSuccess true")
            } else {
                print("isSuccess false")
            }
        }
        
    }
    
    func testWithClouser(_ value: String, _ completion: @escaping(Bool)-> Void) {
        let seconds = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            if value.count > 0 {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func testWithClouser1(_ value: String, _ completion: @escaping(Bool)-> Void) {
        let seconds = 2.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            if value.count > 0 {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    //--------------------------------//
}

extension DeleteRecordVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.personalRecords?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        if !(cell != nil)
        {
            cell = UITableViewCell(style: .value1, reuseIdentifier: "Cell")
        }
        
        let data = self.personalRecords?[indexPath.row]
        cell!.textLabel?.text = "\(data?.firstName ?? "") \(data?.lastName ?? "")"
        cell!.detailTextLabel?.text = "\(data?.age ?? 0)"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.deleteRecord(self.personalRecords![indexPath.row]) { isSuccess in
                if isSuccess {
                    self.personalRecords?.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    print("Deleted")
                } else {
                    self.present(Utility.showAlertWithString("Error in Deleting Object"), animated: true, completion: nil)
                }
            }
        }
    }
}
